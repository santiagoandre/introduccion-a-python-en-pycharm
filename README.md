
# Introduccion Tecnica a Python y a Numpy

## Primer script

para activar el modo interactivo de python simplemente debemos de escribir en el cmd

```bash
python
```

y al activarlo solo debemos de escribir

```bash
print('hola mundo')
```

## Primer programa de python y ejecución

En una carpeta crear el archivo holamundo con extension py.

```python
#holamundo.py
print('hola mundo')
```

y la manera mas narutal de ejecutarlo es escribiendo en el cmd:

```bash
python holamundo.py
```

## Instalación virtualenv

Usar el administrador de paquetes [pip](https://pypi.org/project/pip/) para instalar [virtualenv](https://jarroba.com/entornos-virtuales-de-python-comun-y-anaconda/).

```bash
pip install virtualenv
virtualenv nombre_entorno_virtual
```

## Activar entorno virtual e instalacion de numpy

Para activar el entorno virtual e instalar numpy usaremos los siguientes procedimientos en el cmd.

```bash
nombre_entorno_virtual\scripts\activate.bat
pip install numpy
```

## Introducción a numpy

Crear un archivo con extension py de cualquier nombre, en este caso se llamara main e importar la libreria numpy

```python
#main.py
import numpy as np 
m_1 = np.array([[0, 1, 2], [3, 4, 5]])

m_2 = a = np.arange(10)

print(m_1)
print('_________')
print(m_2)
```

## Practice questions

### Problem A.1

```python
#ProblemA1
import numpy as np 
m_1 = np.ones((6, 4))
m_2 = m_1*2
print(m_2)
```

### Problem A.2

```python
#ProblemA2
import numpy as np 
m_1 = np.eye(6, 4)*2
m_2 = np.ones((6, 4))
m_3 = m_1+m_2
print(m_3)
```

### Problem A.3

```python
#ProblemA3
import numpy as np
m_1 = np.array([[0, 1], [2, 3], [4, 5]])
m_2 = a = np.array([[0, 1, 2], [3, 4, 5]])
print(m_2.dot(m_1)) #m_2*m_1 retorna un error
```

solo se admiten las dos operaciones si las dos matrices son cuadradas y del mismo tamaño

```python
#ProblemA3
import numpy as np
m_1 = np.array([[0, 1], [2, 3]])
m_2 = a = np.array([[0, 1], [3, 4]])
print(m_1*m_2)
print(m_1.dot(m_2))
```
